#!/bin/sh -xe
#
# wget and execute this script, it will do the rest

# COPR expects the spec and tarball in there
results_dir="results"

# upstream libinput configuration
repo="https://gitlab.freedesktop.org/libinput/libinput"
branch="main"
api_endpoint="https://gitlab.freedesktop.org/api/v4/projects/libinput%2Flibinput"
meson_build_url="$repo/raw/$branch/meson.build"
last_commit_url="$api_endpoint/repository/commits/$branch"
tarball="$repo/-/archive/$branch/libinput-$branch.tar.bz2"

# The special repo where I keep the specfile maker script
copr_src_repo="https://gitlab.freedesktop.org/whot/libinput-copr"
specfile="$copr_src_repo/raw/master/libinput.spec.in"

mkdir -p "$results_dir"

date=$(date +%Y%m%d%H%M)
# Extract the current version from meson.build
version=$(curl $meson_build_url | \
          grep -A 3 'project(' |grep '\<version\> :' | \
          sed -e "s|.*version : '\(.*\)',|\1|")
# Extract the current sha from the API
sha=$(curl "$api_endpoint/repository/commits/$branch" | \
    sed -e 's|{"id":"\([[:alnum:]]\{7\}\).*|\1|')

# Put the specfile and the tarball into the results/ directory. COPR will
# pick it up from there and build the rpms.
curl "$specfile" | \
    sed -e "s|@GITDATE@|$date|" \
        -e "s|@VERSION@|$version|" \
        -e "s|@GITVERSION@|$sha|" \
    > "$results_dir/libinput.spec"
curl -o "$results_dir/libinput-$branch.tar.bz2" "$tarball"

%global udevdir %(pkg-config --variable=udevdir udev)

%global gitdate @GITDATE@
%global gitversion @GITVERSION@
%global branch main

Name:           libinput
Version:        @VERSION@
Release:        %{gitdate}git%{gitversion}%{?dist}
Summary:        Input device library

License:        MIT
URL:            http://www.freedesktop.org/wiki/Software/libinput/
Source0:        %{name}-%{branch}.tar.bz2

BuildRequires:  git-core
BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(mtdev) >= 1.1.0
BuildRequires:  pkgconfig(libevdev) >= 0.4
BuildRequires:  pkgconfig(libwacom) >= 0.20
BuildRequires:  pkgconfig(udev)
BuildRequires:  python3-rpm-macros
BuildRequires:  check-devel

%description
libinput is a library that handles input devices for display servers and other
applications that need to directly deal with input devices.

It provides device detection, device handling, input device event processing
and abstraction so minimize the amount of custom input code the user of
libinput need to provide the common set of functionality that users expect.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        utils
Summary:        Utilities and tools for debugging %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       python3-pyudev python3-libevdev

%description    utils
The %{name}-utils package contains tools to debug hardware and analyze
%{name}.

%package        test
Summary:        libinput integration test suite
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    test
The %{name}-test package contains the libinput test suite. It is not
intended to be run by users.

%prep
%autosetup -S git -n %{name}-%{branch}
# Replace whatever the source uses with the approved call
%py3_shebang_fix $(git grep -l  '#!/usr/bin/.*python3')

%build
%meson -Ddebug-gui=false \
       -Ddocumentation=false \
       -Dtests=true \
       -Dinstall-tests=true \
       -Dudev-dir=%{udevdir}
%meson_build

%install
%meson_install

%post
%{?ldconfig}

%ldconfig_postun


%files
%doc COPYING
%{_libdir}/libinput.so.*
%{udevdir}/libinput-device-group
%{udevdir}/libinput-fuzz-extract
%{udevdir}/libinput-fuzz-to-zero
%{udevdir}/rules.d/80-libinput-device-groups.rules
%{udevdir}/rules.d/90-libinput-fuzz-override.rules
%{_bindir}/libinput
%dir %{_libexecdir}/libinput/
%{_libexecdir}/libinput/libinput-debug-events
%{_libexecdir}/libinput/libinput-list-devices
%{_mandir}/man1/libinput.1*
%{_datadir}/libinput/*.quirks
%dir %{_datadir}/zsh
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/*
%{_mandir}/man1/libinput-list-devices.1*
%{_mandir}/man1/libinput-debug-events.1*

%files devel
%{_includedir}/libinput.h
%{_libdir}/libinput.so
%{_libdir}/pkgconfig/libinput.pc

%files utils
%{_libexecdir}/libinput/libinput-debug-tablet
%{_libexecdir}/libinput/libinput-list-kernel-devices
%{_libexecdir}/libinput/libinput-measure
%{_libexecdir}/libinput/libinput-measure-fuzz
%{_libexecdir}/libinput/libinput-measure-touchpad-tap
%{_libexecdir}/libinput/libinput-measure-touchpad-pressure
%{_libexecdir}/libinput/libinput-measure-touch-size
%{_libexecdir}/libinput/libinput-measure-touchpad-size
%{_libexecdir}/libinput/libinput-quirks
%{_libexecdir}/libinput/libinput-record
%{_libexecdir}/libinput/libinput-replay
%{_libexecdir}/libinput/libinput-analyze
%{_libexecdir}/libinput/libinput-analyze-buttons
%{_libexecdir}/libinput/libinput-analyze-per-slot-delta
%{_libexecdir}/libinput/libinput-analyze-recording
%{_libexecdir}/libinput/libinput-analyze-touch-down-state
%{_mandir}/man1/libinput-debug-tablet.1*
%{_mandir}/man1/libinput-list-kernel-devices.1*
%{_mandir}/man1/libinput-measure.1*
%{_mandir}/man1/libinput-measure-fuzz.1*
%{_mandir}/man1/libinput-measure-touchpad-tap.1*
%{_mandir}/man1/libinput-measure-touch-size.1*
%{_mandir}/man1/libinput-measure-touchpad-size.1*
%{_mandir}/man1/libinput-measure-touchpad-pressure.1*
%{_mandir}/man1/libinput-quirks.1*
%{_mandir}/man1/libinput-quirks-list.1*
%{_mandir}/man1/libinput-quirks-validate.1*
%{_mandir}/man1/libinput-record.1*
%{_mandir}/man1/libinput-replay.1*
%{_mandir}/man1/libinput-analyze.1*
%{_mandir}/man1/libinput-analyze-buttons.1*
%{_mandir}/man1/libinput-analyze-per-slot-delta.1*
%{_mandir}/man1/libinput-analyze-recording.1*
%{_mandir}/man1/libinput-analyze-touch-down-state.1*

%files test
%{_libexecdir}/libinput/libinput-test
%{_libexecdir}/libinput/libinput-test-suite
%{_libexecdir}/libinput/libinput-test-utils
%{_mandir}/man1/libinput-test.1*
%{_mandir}/man1/libinput-test-suite.1*

%changelog
* Wed Jun 19 2024 Peter Hutterer <peter.hutterer@redhat.com>
- Add the new libinput analyze-buttons tool

* Fri Jul 21 2023 Peter Hutterer <peter.hutterer@redhat.com>
- Use the py3_shebang_fix rpm macro

* Thu Jul 20 2023 Peter Hutterer <peter.hutterer@redhat.com>
- BuildRequires python3-rpm-macros for pathfix.py in F39 and later

* Mon Dec 12 2022 Peter Hutterer <peter.hutterer@redhat.com>
- New tool: libinput list-kernel-devices

* Wed Aug 31 2022 Peter Hutterer <peter.hutterer@redhat.com>
- Update BuildRequires to include udev.pc

* Tue May 10 2022 Peter Hutterer <peter.hutterer@redhat.com>
- New tool: libinput test
- New tool: libinput test utils

* Tue Feb 23 2021 Peter Hutterer <peter.hutterer@redhat.com>
- New tool: libinput analyze recording

* Fri Jan 29 2021 Peter Hutterer <peter.hutterer@redhat.com>
- New tool: libinput analyze touch-down-state

* Tue Sep 22 2020 Peter Hutterer <peter.hutterer@redhat.com>
- Drop c++ from the BuildRequires, no longer required

* Fri Apr 24 2020 Peter Hutterer <peter.hutterer@redhat.com>
- add libinput measure touchpad-size

* Fri Mar 20 2020 Peter Hutterer <peter.hutterer@redhat.com>
- Update the file list for libinput-analyze

* Thu Sep 12 2019 Peter Hutterer <peter.hutterer@redhat.com>
- Update file list
- Use autosetup -n

* Tue Jul 30 2019 Peter Hutterer <peter.hutterer@redhat.com>
- Update for libinput 1.13

* Wed Sep 12 2018 Peter Hutterer <peter.hutterer@redhat.com>
- remove libinput measure trackpoint-range tool/man page
- add the udev model quirks rules

* Tue Jul 31 2018 Peter Hutterer <peter.hutterer@redhat.com>
- copr autobuild from git setup

